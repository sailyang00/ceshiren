## docker 部署 jenkins
命令行：  
``docker run -d --name jenkins -p 8080:8080 -p 50000:50000 -v jenkins:/var/jenkins_home jenkins/jenkins:latest``

运行中：  
![Image text](https://gitee.com/sailyang00/ceshiren/raw/master/class7/moudle2/ceshiren_homework_class7_moudle2/img/docker_jenkins.png)

## Jenkins配置自动化脚本  
计划配置：
![Image text](https://gitee.com/sailyang00/ceshiren/raw/master/class7/moudle2/ceshiren_homework_class7_moudle2/img/localhost_8080_job_1011_iInterface_python_configure.png)  

执行结果：
![Image text](https://gitee.com/sailyang00/ceshiren/raw/master/class7/moudle2/ceshiren_homework_class7_moudle2/img/localhost_8080_job_1011_iInterface_python_5_console.png)


## Docker+Jenkins+Jmeter  
计划配置：
![Image text](https://gitee.com/sailyang00/ceshiren/raw/master/class7/moudle2/ceshiren_homework_class7_moudle2/img/localhost_8080_job_20211018_stress_test_configure.png) 

执行结果：
![Image text](https://gitee.com/sailyang00/ceshiren/raw/master/class7/moudle2/ceshiren_homework_class7_moudle2/img/localhost_8080_job_20211018_stress_test_17_console.png)  

结果报告：
![Image text](https://gitee.com/sailyang00/ceshiren/raw/master/class7/moudle2/ceshiren_homework_class7_moudle2/img/localhost_8080_job_20211018_stress_test__e887aa_e58aa8_e58c96_e58e8b_e6b58b_5f10_e5b9b6_e58f91_.png)  