"""
单链表
"""


class SinglyLinkedList:
    def __init__(self):
        # 头指针
        self.head = None

    class Node:
        def __init__(self, data):
            self.data = data
            self.next = None

    '''
    插入尾节点
    '''

    def insert_tail(self, value):
        # 先判断链表中是否有值
        if self.head is None:
            # 如果链表为空，将数据插入到头节点
            self.insert_to_head(value)
            return

        # 头节点赋值给变量q
        q = self.head

        # 寻找尾节点
        while q.next is not None:
            q = q.next
        # 找到尾节点后，生成新节点
        new_node = self.Node(value)

        # 将新节点赋值给尾结点next指针，相当于插入了新的尾节点
        q.next = new_node

    '''
    插入头节点
    '''

    def insert_to_head(self, value):
        new_node = self.Node(value)
        # 如果头节点为空，就把新节点赋值给头节点
        if self.head is None:
            self.head = new_node
        else:
            # 如果不为空，就将头节点赋值给新节点的next指针
            # 并且将新节点赋值给头节点，相当于头节点被替换为了新节点
            new_node.next = self.head
            self.head = new_node

    '''
    节点后插入
    '''

    def insert_after(self, node, value):
        if node is None:
            return
        # 生成一个新节点
        new_node = self.Node(value)
        # 原本node.next指针赋值给了新节点的new_node.next
        # 相当于A.next原本指向了C,插入了B节点后，B.next就应该指向C，所以需要将A.next 赋值给 B.next，让B.next指向C
        # 而A.next指向B
        new_node.next = node.next
        node.next = new_node

    '''
    节点前插入：
    与节点后插入的逻辑判断方式不同，最主要原因是节点插入后方法是用两个节点来做逻辑判断
    而节点后插入是通过三个节点来做逻辑判断
    '''

    def insert_before(self, node, value):
        # 判断链表是否为空，如果为空就插入值
        if self.head is None:
            self.insert_to_head(value)
            return
        q = self.head

        # 循环判断链表中q的下一个节点是否为node
        while q is not None and q.next != node:
            q = q.next

        # 如果q为None，则说明，链表中没有找到node节点
        if q is None:
            return

        # 如果q不为None，则生成新节点，相当于在 前驱节点q和查询节点node中间插入新节点
        new_node = self.Node(value)
        # 新节点的next指向node
        new_node.next = node

        # 前驱节点的next指向新节点
        q.next = new_node

    '''
    删除节点：首先要找到前驱节点，然后将前驱节点指向下一个节点
    '''

    def delete_by_value(self, value):
        # 判断链表是否为空
        if self.head is None:
            return False
        # 不为空的话就将头节点赋值给变量q
        q = self.head

        # 声明变量p 为前驱节点
        p = None

        # 如果q 不为空，且当前节点的值不等于删除的value值
        # 将当前节点赋值给前驱节点
        # 下一个节点赋值给当前节点
        while q is not None and q.data != value:
            p = q
            q = q.next

        # 有可能q.next为空，即链表中没有该value值
        if q is None:
            return False

        # 当p为空时，说明没有进入while循坏体，意味着链表中头节点就等于value，也就是要删除头节点
        # 此时self.head.next为None，赋值给self.head相当于删除了头节点
        if p is None:
            self.head = self.head.next
        # 若不为空，说明进入了while循环体，意味着在链表中找到value值，即q.data = value
        # 此时，要删除q节点，只需将前驱节点p.next 指向 q.next
        else:
            p.next = q.next
        return True

    '''
    查找节点
    '''

    def find_by_value(self, value):
        if self.head is None:
            return
        q = self.head

        # 查找链表中是否存在value
        while q is not None and q.data != value:
            q = q.next
        # 如果q为None，即q.next==None，说明链表中没有value值
        if q is None:
            return
        return q

    '''
    打印链表
    '''

    def print_all(self):
        # 判断链表是否为空
        if self.head is None:
            return
        q = self.head
        # 当q不为空就一直打印节点内容
        while q is not None:
            print(q.data)
            # 并且q的next指针赋值给q，否则一直为q值
            q = q.next


def test_link():
    # 实例化链表
    link = SinglyLinkedList()
    data = [1, 2, 3, 4, 1]
    for i in data:
        # 插入尾节点
        link.insert_tail(i)
    # 插入头部
    link.insert_to_head(99)
    link.print_all()

    # 删除
    link.delete_by_value(2)
    # 断言
    # 删除不能存在的值
    assert not link.delete_by_value(999)
    assert link.delete_by_value(99)
    link.print_all()

    # 查找
    assert link.find_by_value(2) is None
    # 生成新节点
    new_node = link.find_by_value(3)
    link.insert_after(new_node, 10)
    assert link.find_by_value(3).next.data == 10
    link.insert_before(new_node, 30)
    assert link.find_by_value(1).next.data == 30
    link.print_all()


if __name__ == '__main__':
    test_link()
