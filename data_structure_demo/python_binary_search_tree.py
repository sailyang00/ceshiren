"""
二叉树（查找，插入，删除）
    用链表实现
    难点：
        1.嵌套类，用Node的类属性存储本身Node实例，通过递归的方式读取链式数据
        2.二叉树节点删除情况分为三种
            a.被删除节点下，仅有左节点或仅有右节点   实现方式；父节点删除后，子节点直接拼接上即可
            b.被删除节点下，没有子节点             实现方式：删除节点即可
            c.被删除节点下，同时存在左右节点        实现方式：被删节点的右子树中最小值替换掉被删节点，随之就成了a情况，最后将最小值删除
        3.方法delete中，p的内存地址存储误区，需要注意的是p是类实例，而不是字符串或整型
            a.如果字符串或整型发生改版的话，对应的id内存地址也会变化。但是p是类实例，改变的仅仅是类属性，并不会改变类实例的id内存地址

"""


class BinarySearchTree:
    # 声明根节点
    def __init__(self):
        self.tree = None

    # 声明节点，拥有属性 左指针和右指针和存储值data
    class Node:
        def __init__(self, data):
            self.data = data
            self.left = None
            self.right = None

    """
    插入
    """

    def insert(self, value):
        # 判断当前树中是否有根节点，如果没有则直接实例一个新节点为根节点
        if self.tree is None:
            self.tree = self.Node(value)
            return

        p = self.tree

        # 当前节点不为空时，比较当前节点的值和插入数据的大小
        while p is not None:
            # 若插入的数据比当前节点大，则往右节点寻找
            # 若右节点为空，则插入节点；若不为空，则继续循环寻找
            if value > p.data:
                if p.right is None:
                    p.right = self.Node(value)
                    return
                p = p.right

            # 若插入的数据比当前节点小，则往左节点寻找
            # 若左节点为空，则插入节点；若不为空，则继续循环寻找
            elif value < p.data:
                if p.left is None:
                    p.left = self.Node(value)
                    return
                p = p.left

    """
    查询
    """

    def find(self, value):
        p = self.tree

        # 若根节点不为空，且寻找值比当前节点大，往右节点寻找，反之往左节点寻找，直到寻找值等于节点值
        # 如果寻找值不在二叉树中，则返回None
        while p is not None:
            if value > p.data:
                p = p.right
            elif value < p.data:
                p = p.left
            else:
                return p
        return None

    def delete(self, value):
        p = self.tree

        # 声明一个父节点
        pp = None

        # 循环查找等于value值的节点，pp为父节点，p为当前节点
        while p is not None and p.data != value:
            pp = p
            if value > p.data:
                p = p.right
            elif value < p.data:
                p = p.left

        # 如果p为None，说明二叉树中没有节点等于value值
        if p is None:
            return

        '''
        运行到这里，说明在二叉树中找到了等于value值的节点
        '''
        # 若当前节点下，左节点和右节点均存在
        while p.left is not None and p.right is not None:

            # 记录当前节点和当前节点的右节点
            tmp_p = p.right
            tmp_pp = p

            # 找到要删除的节点的右子树中的最小值
            while tmp_p.left is not None:
                tmp_pp = tmp_p
                tmp_p = tmp_p.left
            p.data = tmp_p.data
            p = tmp_p
            pp = tmp_pp

        # 目标节点的左子节点不为空，则将左子节点赋值给child
        if p.left is not None:
            child = p.left
        # 目标节点的右子节点不为空，则将左子节点赋值给child
        elif p.right is not None:
            child = p.right
        # 如果都为空，那么child为None
        else:
            child = None

        '''
        如果pp值为None，说明找到的P节点为根节点
        这一步骤相等于删除p值，但这里的逻辑仅考虑当前节点仅有左节点或仅有右节点，又或者都没有子节点
        '''

        # 若当前节点为根节点，删除根节点，让子节点替换上
        if pp is None:
            self.tree = child

        # 若当前节点为父节点的左节点，删除当前节点，让子节点替换上父节点的左节点

        elif pp.left is p:

            pp.left = child

        # 若当前节点为父节点的右节点，删除当前节点，让子节点替换上父节点的右节点
        elif pp.right is p:
            pp.right = child

    '''
    递归遍历Node节点
    '''

    # 前序遍历
    def pre_order(self, node):
        if node is None:
            return
        self.pre_order(node.left)
        self.pre_order(node.right)

    # 中序遍历
    def in_order(self, node):
        if node is None:
            return
        self.in_order(node.left)
        self.in_order(node.right)

    # 后序遍历
    def post_order(self, node):
        if node is None:
            return
        self.post_order(node.left)
        self.post_order(node.right)
        print(node.data)


def test_binary_search_tree():
    binary_search_tree = BinarySearchTree()

    # 插入
    data = [80, 70, 65, 75, 90, 85, 93, 92, 95]
    for i in data:
        binary_search_tree.insert(i)

    # 删除功能校验
    binary_search_tree.delete(90)
    assert binary_search_tree.find(90) is None

    # 遍历
    print("_______________________")
    # 前序遍历 1 10 40 13
    binary_search_tree.pre_order(binary_search_tree.tree)
    print("_______________________")
    # 中序遍历 1 10 13 40
    binary_search_tree.in_order(binary_search_tree.tree)
    print("_______________________")
    # 后序遍历 13 40 10 1
    binary_search_tree.post_order(binary_search_tree.tree)
    print("_______________________")


if __name__ == '__main__':
    test_binary_search_tree()
