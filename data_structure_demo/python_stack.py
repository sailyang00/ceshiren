"""
顺序栈
"""


class ArrayStack:
    def __init__(self, n) -> None:
        self.data = [-1] * n
        self.n = n
        self.count = 0

    # 入栈，检查堆栈长度和已入栈数是否相等，如果相等说明堆栈处于0长度或者满容量
    def push(self, value):
        if self.n == self.count:
            return False
        self.data[self.count] = value
        self.count += 1
        return True

    # 出栈，检查堆栈的已入栈数是否为0，如果不为0才可以进行出栈操作
    def pop(self):
        if self.count == 0:
            return None
        self.count -= 1
        return self.data[self.count]


def test_static():
    # 实例化一个堆栈，长度为5
    array_stack = ArrayStack(5)
    data = ["a", "b", "c", "d", "e"]

    # 将组数中的数据放到堆栈内
    for i in data:
        array_stack.push(i)

    # 放入数据超过长度，返回-1
    result = array_stack.push("a")
    assert not result

    # 将堆栈内的数据翻转，模拟堆栈后进先出的原则
    data.reverse()

    # 将堆栈中的元素按顺序取出来
    for i in data:
        assert i == array_stack.pop()

    # 元素都被取完后，检查堆栈是否为空
    assert array_stack.pop() is None


if __name__ == '__main__':
    test_static()
