"""
链式栈：
链式栈没有长度限制的，所以设置一个头部即可
"""


class StackBasedOnLinkedlist:
    def __init__(self):
        self.top = None

    # 如过栈为空，就生成一个节点当做头部
    # 如果不为空，就将头部节点赋值给下一个节点，重新生成新节点为头部
    def push(self, value):
        new_node = self.Node(value)
        if self.top is None:
            self.top = new_node
        else:
            new_node.next = self.top
            self.top = new_node

    # 如果头部为空，无法取出数据，返回-1
    # 如果不为空，将头部数据返回，下一个节点变为头部
    def pop(self):
        if self.top is None:
            return -1
        result = self.top.data
        self.top = self.top.next
        return result

    """
    声明节点类，该节点实例有两个属性，data和next
    分别表示该节点有数据并且记录了下一个节点的地址
    """

    class Node:
        def __init__(self, data):
            self.data = data
            self.next = None


def test_static():
    # 生成链式栈实例
    stack = StackBasedOnLinkedlist()
    data = [1, 2, 3, 4, 5]
    for i in data:
        stack.push[i]
    data.reverse()
    for i in data:
        assert i == stack.pop()

    # 当链式栈中数据为空时，返回-1
    assert stack.pop() == -1
