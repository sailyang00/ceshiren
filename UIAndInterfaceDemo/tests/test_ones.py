# @Time : 2022/3/2 11:25 PM 
# @Author : SailYang
import json
import time

import pytest
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


# 获取的cookies转成Selenium能识别的格式
def cookie_to_selenium_format(cookie):
    cookie_selenium_mapping = {'path': '', 'secure': '', 'name': '', 'value': '', 'expires': ''}
    cookie_dict = {}
    if getattr(cookie, 'domain_initial_dot'):
        cookie_dict['domain'] = '.' + getattr(cookie, 'domain')
    else:
        cookie_dict['domain'] = getattr(cookie, 'domain')
    for k in list(cookie_selenium_mapping.keys()):
        key = k
        value = getattr(cookie, k)
        cookie_dict[key] = value
        return cookie_dict


class TestOneAI:
    def setup_method(self, method):
        self.s = requests.Session()
        self.login_url = 'https://ones.ai/project/api/project/auth/login'
        self.home_page = 'https://ones.ai/project/#/home/project'
        self.header = {
            "content-type": "application/json;charset=UTF-8",
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 "
                          "(KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36"
        }
        self.driver = webdriver.Chrome()

    # 使用接口的方式获取登录态，再进行Selenium/webdriver 操作
    @pytest.mark.parametrize('login_data,project_name',
                             [({"password": "iTestingIsGood", "email": "pleasefollowTesting@outlook.com"},
                               {"project_name": "VIPTEST"})])
    def test_merge_api_ui(self, login_data, project_name):
        result = self.s.post(self.login_url, data=json.dumps(login_data), headers=self.header)
        time.sleep(5)
        assert result.status_code == 200
        assert json.loads(result.text)["user"]["email"].lower() == login_data["email"]

        all_cookies = self.s.cookies._cookies[".ones.ai"]["/"]
        self.driver.get(self.home_page)
        self.driver.delete_all_cookies()

        # 获取cookies，记录登录态
        for k, v in all_cookies.items():
            self.driver.add_cookie(cookie_to_selenium_format(v))

        self.driver.get(self.home_page)

        try:
            element = WebDriverWait(self.driver, 30).until(
                expected_conditions.presence_of_element_located((By.CSS_SELECTOR, '[class="company-title-text"]'))
            )
            assert element.get_attribute('innerHTML') == project_name["project_name"]
        except TimeoutError:
            raise TimeoutError('Run time out')

    def teardown_method(self, method):
        self.s.close()
        self.driver.quit()


if __name__ == '__main__':
    pytest.main(['-v', '-s', '__file__'])
