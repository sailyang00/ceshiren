# @Time : 2022/3/13 11:38 AM 
# @Author : SailYang
import time

from selenium import webdriver

from MultiprocessTestDemo.common.data_provider import data_provider
from MultiprocessTestDemo.common.test_decorator import Test
from MultiprocessTestDemo.pages.baidu import Baidu


class BaiduTest(object):
    @data_provider(
        [('testing1', 'testing1'), ('testing4', 'testing4')]
    )
    @Test(tag='smoke')
    def test_baidu_search(self, *data):
        # 启动浏览器，并远程指定操作系统为win10，浏览器为Chrome
        # self.driver = webdriver.Remote(
        #     command_executor='http://192.168.0.199:4444/wd/hub',
        #     desired_capabilities={
        #         'browserName': 'chrome',
        #         'javascriptEnable': 'True',
        #         'platformName': 'Win10'
        #     }
        # )

        # baidu = Baidu(self.driver)
        # 如果本地运行注释掉上面的代码，即不远程控制执行
        baidu = Baidu()

        search_string, expect_string = data
        results = baidu.baidu_search(search_string)
        print(expect_string, results)
        assert expect_string in results

        baidu.close()
