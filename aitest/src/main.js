import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import api from './api/api'
import common from '../src/components/common'
import * as echarts from 'echarts'

Vue.config.productionTip = false
// 如果需要在页面上使用到api.js 就需要在main.js配置
// 所有需要在页面上加载并用到js或组件 都需要在main.js配置
// prototype设置作用域，让$api在所有的Vue实例中可用，甚至是在实例被创建之前
Vue.prototype.$api = api
Vue.component('head-view',common)

// 将echarts引入，并添加到Vue原型上
Vue.prototype.$echarts = echarts

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
