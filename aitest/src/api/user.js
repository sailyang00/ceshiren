import axios from './http'

// 定义user变量
const user = {
    // 定义signIn post方法
    signIn(params) {
        return axios.post('/user/login',params)
    },
    // 定义singUp post方法
    signUp(params) {
        return axios.post('user/register',params)
    }
}
export default user