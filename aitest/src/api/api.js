import user from './user'
import cases from './cases'
import task  from './task'

// 定义api变量
// 并导入user，后续可以在api中直接引用user的方法
const api = {
    user,
    cases,
    task,
}

export default api