import axios from './http'

const task = {
    createTask(params) {
        return axios.post('/task/', params)
    }
}
export default task