import axios from 'axios'
var instance = axios.create({
    headers: {
        'Content-Type': 'application/json'
    },
    baseURL: 'http://stuq.ceshiren.com:8089/'
})
// 局部拦截器，当使用到axios请求时才会被拦截。一般常用做修改请求的方法
// 检查当前localStorage中是否存在token的值，有就取出并赋值给headers中commen里的token键值
instance.interceptors.request.use(config => {
    if (localStorage.getItem('token')) {
        config.headers.common['token'] = localStorage.getItem('token')
    }
    return config
})
export default instance