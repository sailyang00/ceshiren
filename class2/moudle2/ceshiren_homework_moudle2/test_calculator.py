# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/6/25 5:36 下午

import allure
import pytest

from ceshiren_homeword_module2.utlis import readout, ids, UserLog


@allure.feature("计算器测试")
class TestCal:
    """
    计算测试用例
    """

    @classmethod
    def setup_class(cls):
        """
        类测试用例执行前先创建日志系统
        :return: None
        """
        cls.log = UserLog()
        cls.logger = cls.log.get_log()

    @allure.story("加法")
    @pytest.mark.parametrize(
        "a,b,expect",
        readout('plus'),
        ids=ids('plus', readout('plus'))
    )
    def test_cal_plus(self, a, b, expect, cal_init):
        plus_result = cal_init.cal_plus(a, b)
        # 记录日志
        self.logger.info(f"{a}+{b}={plus_result}  expect:{expect}")

        assert expect == plus_result

    @allure.story("减法")
    @pytest.mark.parametrize(
        "a,b,expect",
        readout('sub'),
        ids=ids('sub', readout('sub'))
    )
    def test_cal_sub(self, a, b, expect, cal_init):
        plus_result = cal_init.cal_sub(a, b)
        self.logger.info(f"{a}-{b}={plus_result}  expect:{expect}")
        assert expect == plus_result

    @allure.story("乘法")
    @pytest.mark.parametrize(
        "a,b,expect",
        readout('mul'),
        ids=ids('mul', readout('mul'))
    )
    def test_cal_mul(self, a, b, expect, cal_init):
        plus_result = cal_init.cal_mul(a, b)
        self.logger.info(f"{a}*{b}={plus_result}  expect:{expect}")
        assert expect == plus_result

    @allure.story("除法")
    @pytest.mark.parametrize(
        "a,b,expect",
        readout('div'),
        ids=ids('div', readout('div'))
    )
    def test_cal_div(self, a, b, expect, cal_init):
        _plus_result = cal_init.cal_div(a, b)
        # 检查结果是否浮点数，保留两位
        if isinstance(_plus_result, float):
            plus_result = round(_plus_result, 2)
        self.logger.info(f"{a}/{b}={plus_result}  expect:{expect}")
        assert expect == plus_result


