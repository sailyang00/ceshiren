# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/6/25 4:59 下午


class Calculator:
    """
    计算器的计算方法：加减乘除
    """
    @staticmethod
    def cal_plus(a, b):
        return a + b

    @staticmethod
    def cal_sub(a, b):
        return a - b

    @staticmethod
    def cal_mul(a, b):
        return a * b

    @staticmethod
    def cal_div(a, b):
        return a / b



