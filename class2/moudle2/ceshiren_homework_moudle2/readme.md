
#解题思路：
        ├── __init__.py 
        ├── calculator.py        主要放置计算器方法
        ├── conftest.py          创建了allure指令，用于生成报告；还有一些执行前先调用的方法
        ├── datas.yaml           数据驱动，管理计算的数据
        ├── log                  置放日志文件
        │    └── test.log
        ├── pytest.ini           日志配置，指令配置
        ├── requirement.text     项目所需的依赖文件
        ├── test_calculator.py   执行用例
        └── utlis.py             工具放置文件

    该项目可以执行完测试用例后，自动生成报告。
    计算所用的数据由yaml进行管理。
    测试用例所需要的工具放在另一个文件utlis
    只需要在终端输入 pytest ，项目会自动运行测试用例并生成报告
