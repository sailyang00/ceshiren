# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/6/26 3:42 下午
import logging
from time import sleep

import pytest
import os

from ceshiren_homeword_module2.calculator import Calculator


@pytest.fixture(scope="class")
def cal_init(cmdoption):
    # 用例执行前调用
    print("\n 计算开始")
    calculator = Calculator()
    yield calculator
    # 测试用例执行后调用
    print("\n 计算结束")


def pytest_addoption(parser) -> None:
    # 在pytest --help 中增加一个group  名为 allure
    mygroup = parser.getgroup("allure")

    # 注册一个命令行选项 --allure
    mygroup.addoption("--allure",
                      default="./result",
                      help="set your allure report dir",
                      )


# 用来执行allure 命令，生成报告并查看
@pytest.fixture(scope='session')
def cmdoption(request):
    dir = request.config.getoption("--allure", default="./result")
    return os.system(f"allure serve {dir}")
