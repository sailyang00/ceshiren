# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/6/26 3:20 下午
import logging

import yaml

"""
工具文件
"""

# 映射加减乘除符号
dic = {
    "plus": "+",
    "sub": "-",
    "mul": "*",
    "div": "/"
}


# 数据驱动，读取计算数据
def readout(method):
    try:
        with open("./datas.yaml") as f:
            data = yaml.safe_load(f)[f'{method}']
    except KeyError:
        print("配置文件里没有定义该运行方法和数据")
        return None
    return data


# 用来传递给 pytest.mark.parametrize() 形参中ids
def ids(method, data):
    if method in dic.keys():
        _ids = [f"a:{a} {dic[method]} b:{b} = expect:{expect}" for a, b, expect in data]
    return _ids


# 封装日志系统
class UserLog:
    def __init__(self, logging=logging):
        self.logging = logging
        self.logging.basicConfig(level=logging.INFO)
        self.logger = self.logging.getLogger(__name__)

    def get_log(self):
        return self.logger


