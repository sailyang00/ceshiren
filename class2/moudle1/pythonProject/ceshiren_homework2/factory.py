# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/6/18 4:26 下午
from hero_create import Hero


class Factory:
    """
    从工厂中生产英雄，仅需传入英雄名字，程序会自动到配置文件搜索该英雄属性并生产英雄实例
    后续想要生产其他英雄，仅需到配置文件中配置其他英雄的属性即可
    """

    @staticmethod
    def hero_crete(name: str) :
        return Hero(name)


if __name__ == '__main__':
    factory = Factory()
    Timo = factory.hero_crete("Timo")
    Jinx = factory.hero_crete("Jinx")
    Timo.fight(Jinx.power, Jinx.hp)
