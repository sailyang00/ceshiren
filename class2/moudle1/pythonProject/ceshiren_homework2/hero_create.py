# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/6/18 3:15 下午
import yaml

"""
题目内容：

1. ⼀个回合制游戏，有两个英雄，分别以两个类进⾏定义。分别是Timo和Jinx。每个英雄都有 hp 属性和 power属性，hp 代表⾎量，power 代表攻击⼒
2. 每个英雄都有⼀个 fight ⽅法， fight ⽅法需要传⼊“enemy_hp”（敌⼈的⾎量），“enemy_power”（敌⼈的攻击⼒）两个参数。需要计算对打一轮过后双方的最终血量，
英雄最终的⾎量 = 英雄hp属性-敌⼈的攻击⼒enemy_power
敌⼈最终的⾎量 = 敌⼈的⾎量enemy_hp-英雄的power属性

3. 对⽐英雄最终的⾎量和敌⼈最终的⾎量，⾎量剩余多的⼈获胜。如果英雄赢了打印 “英雄获胜”，如果敌⼈赢了，打印“敌⼈获胜”

4. 使用继承、简单工厂方法等各种方式优化你的代码

解题思路：
实现题目中的问题并不难，所以我将实现的方式进行了升级。
1.Timo和Jinx 两个类的类变量和类方法基本一致，除了power和hp属性的值不同。所以将这两个类合并成一个类，而其类变量由输入参数决定，
这样避免了后续想要继续增加英雄，就要增加英雄类的情况，减少冗代码。

2.将需要生成的英雄参数power和hp放到配置文件方便管理，参数配置一目了然，后续更改参数方便，
例如：增加英雄或者删除英雄，更改属性参数
 
"""


class HeroCreate:
    def __init__(self, power: int, hp: int, name: str):
        """
        1.初始化英雄基本参数
        2.定义了fight实例方法
        :param power: 攻击力
        :param hp: 血量
        :param name: 英雄名字
        """
        self.power = power
        self.hp = hp
        self.name = name
        # 实例生成时调用__call__打印产生英雄的属性
        self.__call__()

    def __call__(self, *args, **kwargs):
        print(f"英雄产生：{self.name}")
        print(f"攻击力: {self.power}")
        print(f"血量: {self.hp}")

    def fight(self, enemy_hp: int, enemy_power: int):
        hero_last_hp = self.hp - enemy_power
        enemy_last_hp = enemy_hp - self.power

        # 判断逻辑
        if hero_last_hp > enemy_last_hp:
            print("Hero win!")
        elif enemy_last_hp > hero_last_hp:
            print("Enemy win!")
        else:
            print("Hero and Enemy drew")


class LoadHeroInfo:
    """
    从配置文件读取英雄的属性，name，power，hp
    """

    @staticmethod
    def load_hero_info():
        with open('hero_properties', encoding="UTF-8") as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        # 检查power和hp是否int属性类型，否则主动报错并打印报错位置信息
        for i in data:
            for j in data[i]:
                for k in j.values():
                    if not isinstance(k, int):
                        raise Exception(f"{j}，请输入整型数字")
        return data


class Hero(HeroCreate):
    # 读取配置文件英雄属性信息
    data = LoadHeroInfo.load_hero_info()

    # 创建实例时输入英雄名字
    def __new__(cls, name):
        # 在配置文件里查询该英雄名字的属性信息，没有则提示英雄不存在
        if name in cls.data:
            cls.power = cls.data[name][0]['power']
            cls.hp = cls.data[name][1]['hp']
            return super().__new__(cls)
        else:
            raise Exception(f'{name} 英雄暂不存在，请前往配置文件添加')

    # 继承父类的init方法，并传入从配置文件里读取的英雄属性
    def __init__(self, name):
        self.name = name
        super().__init__(self.power, self.hp, self.name)
