# @Time : 2022/3/8 7:00 PM 
# @Author : SailYang
from collections import OrderedDict

from TestMarkFilterDemo.common.test_case_finder import DiscoverTestCases
from TestMarkFilterDemo.common.test_filter import TestFilter
from TestMarkFilterDemo.common.user_options import parser_options
from TestMarkFilterDemo.configs.global_config import set_config, config_init


def group_test_cases_by_class(cases_to_run):
    """
    dict字典是无序的，所以存储的数据再迭代时会随机取值，导致顺序随机
    collections.OrderedDict() 方法可以对字典按顺序存取
    """
    test_group_dict = OrderedDict()
    for item in cases_to_run:
        tag_filter, cls, func_name, func, value = item

        # setdefault() 设置键和值的类型，并按顺序存入
        test_group_dict.setdefault(cls, []).append((tag_filter, cls, func_name, func, value))

        # 将测试数据按顺序取出
        test_groups = [(x, y) for x, y in zip(test_group_dict.keys(), test_group_dict.values())]
    return test_groups


def main(args=None):
    options = parser_options(args)

    # 初始化全局变量
    config_init()

    # 设置全局变量
    set_config('config', options.config)

    # 从默认文件tests查找测试用例
    case_finder = DiscoverTestCases()

    # 查找到测试模块并导入
    test_module = case_finder.find_test_module()

    # 查找并筛选测试用例
    original_test_cases = case_finder.find_tests(test_module)

    # 根据用户命令行输入 -i 参数进一步筛选
    raw_test_suites = TestFilter(original_test_cases).tag_filter_run(options.include_tags_any_match)

    # 获取最终的测试用例，并按类名组织
    test_suites = group_test_cases_by_class(raw_test_suites)

    # 运行每一个用例
    for test_suite in test_suites:
        test_class, func_run_pack_list = test_suite
        for func_run_pack in func_run_pack_list:
            cls_group__name, cls, func_name, func, value = func_run_pack
            cls_instance = cls()
            if value:
                getattr(cls_instance, func.__name__).__wrapped__(cls_instance, *value)
            else:
                getattr(cls_instance, func.__name__).__wrapped__(cls_instance)


if __name__ == '__main__':
    main("-env prod -i smoke -t ./tests")
