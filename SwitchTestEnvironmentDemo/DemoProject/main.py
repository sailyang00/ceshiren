# @Time : 2022/3/7 3:48 PM 
# @Author : SailYang
from SwitchTestEnvironmentDemo.DemoProject.common.user_options import parser_options
from SwitchTestEnvironmentDemo.DemoProject.configs.global_config import config_init, set_config
from SwitchTestEnvironmentDemo.DemoProject.tests.test_demo import DemoTest


def main(args=None):
    # 命令行参数获取配置信息
    options = parser_options(args)

    # 初始化全局变量
    config_init()

    # 将命令行获取的配置信息 设置到环境变量中
    set_config('config', options.config)

    # 直接运行测试用例
    demo = DemoTest()
    demo.test_demo_data_driven(('test',))


if __name__ == '__main__':
    main('-t ./tests -env qa')
