# @Time : 2022/3/7 3:47 PM 
# @Author : SailYang
from SwitchTestEnvironmentDemo.DemoProject.common.data_provider import data_provider
from SwitchTestEnvironmentDemo.DemoProject.configs.global_config import get_config


class DemoTest:
    @data_provider([('testing1',), ('testing2',)])
    def test_demo_data_driven(self, data):
        print(f"测试方法为{data}")
        print(f"测试环境各变量为{get_config('config')}")
