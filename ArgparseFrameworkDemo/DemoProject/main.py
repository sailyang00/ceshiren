# @Time : 2022/3/5 7:50 PM 
# @Author : SailYang
import argparse
import os
import shlex
import sys


# """
# 测试框架中，交互式命令行应该拥有最基本的几种可执行的用例模式
# 1)-i，运行指定标签的测试用例（任意匹配），如果一个测试用例A有两个标签，a、b，运行交互式命令时传入 -i a或-i b ，测试用例A都会被执行
# 2）-ai，运行指定标签的测试用例（完全匹配），如果一个测试用例A有两个标签，a、b，运行交互式命令时传入 -ai a,b ，测试用例A才会被执行
# 3）-I，运行指定标签的测试用例（任意匹配），如果一个测试用例A有两个标签，a、b，运行交互式命令时传入 -I a或-I b，只要没有其他标签控制测试用例A都会被执行
# 4）-e，剔除指定标签的测试用例（任意匹配）
# 5）-ae，剔除指定标签的测试用例（完全匹配）
# 6）-E，剔除指定标签的测试类下的所有测试用例（任意匹配）
# """

def parser_options(user_options=None):
    parser = argparse.ArgumentParser(prog="testing", description="testing framework demo")

    parser.add_argument("-t", action="store", default="." + os.sep + "tests", dest="test_targets",
                        metavar="target run path/file",
                        help="Specify run path/file")
    include_tag = parser.add_mutually_exclusive_group()

    include_tag.add_argument("-i", action="store", default=None, dest="include_tags_any_match",
                             metavar="user provided tags(Any)", help="Specify tags to run")
    include_tag.add_argument("-ai", action="store", default=None, dest="include_tags_full_match",
                             metavar="user provided tags(Full Match)", help="Specify tags to run,full match")

    exclude_tags = parser.add_mutually_exclusive_group()
    exclude_tags.add_argument("-e", action="store", default=None, dest="exclude_tags_any_match",
                              metavar="user exclude tags(Any)", help="Exclude tags to run")
    exclude_tags.add_argument("-ae", action="store", default=None, dest="exclude_tags_full_match",
                              metavar="user exclude tags(Full Match)", help="Exclude tags to run,full match")

    all_include_tag = parser.add_mutually_exclusive_group()
    all_include_tag.add_argument("-I", action="store", default=None, dest="exclude_groups_any_match",
                                 metavar="user provided class tags(Any)", help="Specify class to run")

    all_exclude_tag = parser.add_mutually_exclusive_group()
    all_exclude_tag.add_argument("-E", action="store", default=None, dest="exclude_groups_any_match",
                                 metavar="user exclude class tags(Any)", help="Exclude class to run")

    if not user_options:
        args = sys.argv[1:]

    else:
        args = shlex.split(user_options)

    # parser.parse_known_args() 方法与parse_args()作用类似，但与前者相比它接收不了多余的命令行参数，否则会报错。
    # parser.parse_known_args() 可以接收多余的命令行参数，返回一个tuple类型的命名空间和一个保存多余命令的list
    options, un_known = parser.parse_known_args(args)
    return os.path.abspath(options.test_targets)


def main(args=None):
    return parser_options(args)


if __name__ == '__main__':
    print(main())
