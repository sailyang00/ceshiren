# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/8/10 5:12 下午
import json

# import mitmproxy.http
import mitmproxy
from mitmproxy import ctx, http
from mitmproxy.tools.main import mitmdump


class HTTPEvents:
    """
    设备与平台：ios手机，雪球app最新版，mac
    环境：mitmproxy==7.0.2  python==3.9 （注：与教程提供版本不同）
    实现流程：抓取app股票详情的数据包，更改股票数值，从而达到不同的数值在前端逻辑渲染的情况（颜色变化）
    注意：经测试，最新版的雪球app控制颜色变化的参数是 chg 数值，该数值 大于0 小于0 等于0 三种颜色；并不是传统的 percent 值
    """

    def request(self, flow: mitmproxy.http.HTTPFlow):
        """
        实现方法一：
        1.构造伪数据quotec.json文件
        2.将返回数据更改成目标参数
        @param flow: 抓取的Http请求数据包
        @return: None
        """
        # 判断是否抓取到目标接口url
        if "realtime/quotec.json" in flow.request.url:
            # 读取文件中的伪数据
            with open("../files/data.json", "r", encoding="utf-8") as f:
                # json转成字典
                data = json.loads(f.read())

                # 将第一组中的chg值设置成负数，预期绿颜色
                data['data'][0]['chg'] = '-1'
                # 将第二组中的chg值设置成0，预期灰色
                data['data'][1]['chg'] = '0'
                # 将第三组中的chg值设置成正数，预期红颜色
                data['data'][2]['chg'] = '1'

                # 自行构造回包
                # 最新版的mitmproxy调用回包参数是Response，而不是HTTPResponse
                flow.response = http.Response.make(
                    200,

                    # 将字典数据转成json格式再发送出去
                    json.dumps(data),
                    {"content-type": "application/json"}
                )

    def response(self, flow: mitmproxy.http.HTTPFlow):
        """
        实现方法二：
        1.抓取对应请求的返回包
        2.将数据进行更改成目标参数，再返回给客户端，客户端渲染达到测试目的
        @param flow: 抓取的Http返回数据包
        @return: None
        """
        # 判断是否抓取到目标接口url的返回包
        if "realtime/quotec.json" in flow.request.url:
            res_text = flow.response.text
            # 打印返回包数据
            ctx.log(f"response: {flow=}")

            # 将json数据转为字典
            res_text_dic = json.loads(res_text)

            # 将第一组中的chg值设置成负数，预期绿颜色
            res_text_dic['data'][0]['chg'] = '-1'
            # 将第二组中的chg值设置成0，预期灰色
            res_text_dic['data'][1]['chg'] = '0'
            # 将第三组中chg值设置成正数，预期红颜色
            res_text_dic['data'][2]['chg'] = '1'

            # 将字典转成json，并传给返回包
            flow.response.text = json.dumps(res_text_dic)

            # 打印更改后的返回包
            ctx.log(f"quotec.json: {flow.response.text}")


addons = [
    HTTPEvents()
]

if __name__ == '__main__':
    """
    注：如果抓包过程中遇到报错：
    Cannot validate certificate hostname without SNI
    则在启动的时候加上参数 -k 
    """
    mitmdump(['-k', '-s', __file__])
