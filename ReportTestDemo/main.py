# !/usr/bin/python
# -*- coding: utf-8 -*-
# @Time : 2022/3/12 11:00 PM
# @Author : SailYang

import functools
import os
import shutil
import subprocess
import traceback
from collections import OrderedDict
from multiprocessing.pool import ThreadPool
import platform
import time

from ReportTestDemo.common.customize_error import RunTimeTooLong
from ReportTestDemo.common.html_report import format_time, generate_html_report
from ReportTestDemo.common.my_logger import MyLogger
from ReportTestDemo.common.test_case_finder import DiscoverTestCases
from ReportTestDemo.common.test_filter import TestFilter
from ReportTestDemo.common.user_options import parser_options
from ReportTestDemo.configs.global_config import config_init, set_config, get_config

# 初始化日志
log = MyLogger(file_name='./logs/debug_info.log')


def kill_process_by_name(process_name):
    try:
        if platform.system() == "Windows":
            subprocess.check_output(f"taskkill /f /im {process_name}", shell=True, stderr=subprocess.STDOUT)

        # Mac
        elif platform.system() == "Darwin":
            subprocess.check_output(f"ps -ef | grep -i {process_name} | awk {'print $2'} | awk 'NR==1'|xargs kill -9",
                                    shell=True, stderr=subprocess.STDOUT)
        else:
            # 如果是mac的话
            subprocess.check_output(f"killall {process_name}", shell=True, stderr=subprocess.STDOUT)
    except BaseException as e:
        log.error(e)


def clear_env():
    # if platform.system() == "Windows":
    #     kill_process_by_name("chrome.exe")
    #     kill_process_by_name("chromedriver.exe")
    #     kill_process_by_name("firefox.exe")
    #     kill_process_by_name("iexplore.exe")
    #     kill_process_by_name("IEDriverServer.exe")
    # else:
    #     kill_process_by_name("Google Chrome")
    #     kill_process_by_name("chromedriver")
    pass


def group_test_cases_by_class(cases_to_run):
    """
    dict字典是无序的，所以存储的数据再迭代时会随机取值，导致顺序随机
    collections.OrderedDict() 方法可以对字典按顺序存取
    """
    test_group_dict = OrderedDict()
    for item in cases_to_run:
        tag_filter, cls, func_name, func, value = item

        # setdefault() 设置键和值的类型，并按顺序存入
        test_group_dict.setdefault(cls, []).append((tag_filter, cls, func_name, func, value))

        # 将测试数据按顺序取出
    test_groups = [(x, y) for x, y in zip(test_group_dict.keys(), test_group_dict.values())]
    return test_groups


def class_run(case, test_thread_number):
    # 并发寻找测试用例
    cls, func_pack = case
    log.debug(f'类 -{cls.__name__}- 开始运行')
    p = ThreadPool(test_thread_number)
    p.map(func_run, func_pack)
    p.close()
    p.join()
    log.debug(f'类 -{cls.__name__}- 结束运行')


def func_run(case):
    try:
        # 开始时间
        s = time.time()
        cls_group_name, cls, func_name, func, value = case
        log.debug(f'类 -{cls.__name__} 的 方 法 {func_name}- 开始运行')
        cls_instance = cls()

        f_c = dict()
        set_up_method = None
        tear_down_method = None

        for item in dir(cls_instance):
            if getattr(getattr(cls_instance, item), '__test_case_fixture_type__', None) == "__setUp__" and \
                    getattr(getattr(cls_instance, item), "__test_case_fixture_enabled__", None):
                set_up_method = getattr(cls_instance, item)
            if getattr(getattr(cls_instance, item), '__test_case_fixture_type__', None) == "__tearDown__" and \
                    getattr(getattr(cls_instance, item), "__test_case_fixture_enabled__", None):
                tear_down_method = getattr(cls_instance, item)

        # 执行顺序为 前置-测试用例-后置
        if set_up_method:
            set_up_method.__wrapped__(cls_instance)
        if value:
            getattr(cls_instance, func.__name__).__wrapped__(cls_instance, *value)
        else:
            getattr(cls_instance, func.__name__).__wrapped__(cls_instance)

        if tear_down_method:
            tear_down_method.__wrapped__(cls_instance)

        setattr(func, 'run_status', 'pass')

        # 运行结束时间
        e = time.time()
        log.debug(f'类 -{cls.__name__} 的 方 法 {func_name}- 结束运行')
        if e - s > get_config('config')["run_time_out"]:
            raise RunTimeTooLong(func_name, e - s)
    except RunTimeTooLong as runtime_err:

        # 当出现RunTimeTooLong错误时，设置相应的属性
        log.error(runtime_err)
        setattr(func, 'run_status', 'error')
        setattr(func, 'exception_type', 'RunTimeLong')
        setattr(func, 'error_message', repr(traceback.format_exc()))
    except AssertionError as assert_err:
        log.error(assert_err)
        setattr(func, 'run_status', 'fail')
        setattr(func, 'exception_type', 'AssertionError')
        setattr(func, 'error_message', repr(traceback.format_exc()))
    except Exception as e:
        log.error(e)
        setattr(func, 'run_status', 'error')
        setattr(func, 'exception_type', 'Exception')
        setattr(func, 'error_message', repr(traceback.format_exc()))
    finally:
        # setdefault 查到 key 值，如果不存在就把 None 设置为该 key 的值，并返回值
        if not f_c.setdefault("execution_time", None):
            # 当try语句运行出错后，重新获取测试用例结束时间
            e = time.time()
            f_c["execution_time"] = format_time(e - s)
        f_c["testcase_id"] = getattr(cls_instance, 'test_case_id', None)
        f_c["testcase"] = cls.__name__ + '.' + func_name
        f_c["running_testcase_name"] = func_name
        f_c["class_name"] = cls.__name__
        f_c['module_name'] = func.__name__
        # f_c["testcase_filter_tag"] = getattr(func, 'tag', None)

        f_c["exception_type"] = getattr(func, 'exception_type', None)
        f_c["exception_info"] = getattr(func, 'error_message', None)
        f_c["re_run"] = None
        f_c["screenshot_list"] = []

        # 收集测试成功用例
        if getattr(func, 'run_status') == 'pass':
            cases_run_success.append(f_c)
        else:
            # 如果测试运行不成功，则记录日志消息
            func_log_path = os.path.join(out_put_folder, func_name)
            if not os.path.exists(func_log_path):
                os.mkdir(func_log_path)

            func_log_file = os.path.join(func_log_path, 'first_run_log.log')
            with open(func_log_file, 'w+', encoding="utf-8") as f:
                f.writelines(repr(getattr(func, 'error_message', None)))

            # setdefault 用法：设置 key 和 value，value 默认为 None
            # setdefault 方法 返回 value，所以后面接上.append 实际是在对[]进行操作,f_c == {'log_list':[]}
            f_c.setdefault('log_list', []).append('.%s%s%sfirst_run_log.log' % (os.sep, func_name, os.sep))

            # 收集断言错误的测试用例
            if getattr(func, 'run_status') == 'fail':
                cases_run_fail.append(f_c)
            else:
                # 收集运行时发生错误的测试用例
                cases_encounter_error.append(f_c)
        # 环境清理
        clear_env()


def main(args=None):
    options = parser_options(args)

    # 初始化全局变量
    config_init()

    # 设置全局变量
    set_config('config', options.config)

    # 把环境变量写入配置，供测试报告调用
    set_config(get_config('config').setdefault('env', options.default_env), options.default_env)

    # 从默认文件tests查找测试用例
    case_finder = DiscoverTestCases()

    # 查找到测试模块并导入
    test_module = case_finder.find_test_module()

    # 查找并筛选测试用例
    original_test_cases = case_finder.find_tests(test_module)

    # 根据用户命令行输入 -i 参数进一步筛选
    raw_test_suites, excluded_test_suites = TestFilter(original_test_cases).tag_filter_run(
        options.include_tags_any_match)

    # 获取最终的测试用例，并按类名组织
    test_suites = group_test_cases_by_class(raw_test_suites)

    # 获取到被忽略的测试用例集，并按class名组织
    excluded_test_suites = group_test_cases_by_class(excluded_test_suites)

    # 直接解析被忽略的测试用例集，输出各项预设信息供测试报告调用
    for s in excluded_test_suites:
        func_tag, func_list = s
        for item in func_list:
            no_run_cls_group, no_run_cls, no_run_cls_name, no_run_func, value = item
            no_run_dict = dict()
            no_run_dict["testcase_id"] = getattr(no_run_cls, 'test_case_id', None)
            no_run_dict["testcase"] = no_run_cls.__name__ + '.' + no_run_func.__name__
            no_run_dict["execution_time"] = None
            no_run_dict["running_testcase_name"] = None
            no_run_dict["class_name"] = no_run_cls.__name__
            no_run_dict['module_name'] = no_run_cls.__name__
            no_run_dict['re_run'] = None
            no_run_dict['log_list'] = None
            # no_run_dict["testcase_filter_tag"] = getattr(no_run_cls, 'tag', None)
            no_run_dict["exception_type"] = None
            no_run_dict["exception_info"] = None
            no_run_dict["screenshot_list"] = []
            skipped_cases.append(no_run_dict)

    log.debug("日志开始")
    log.debug(f"运行的线程数为：{options.test_thread_number}")

    # print(f"运行线程数为:{options.test_thread_number}")

    p = ThreadPool(options.test_thread_number)

    # functools.partial 偏函数
    # functools.partial(func,x)，x为func的入参参数 ，在map函数里 test_suites 也是func的入参
    # class_run 也是线程池并发，所以这里有两层并发，第一层是查找测试模块，第二层是查找测试用例
    p.map(
        functools.partial(
            class_run,
            test_thread_number=options.test_thread_number
        ), test_suites
    )
    p.close()
    p.join()


if __name__ == '__main__':
    start = time.time()

    # 定义测试运行成功、失败、错误、以及忽略的测试用例集
    cases_run_success = []
    cases_run_fail = []
    cases_encounter_error = []
    skipped_cases = []

    base_out_put_folder = os.path.abspath(os.path.dirname(__name__)) + os.sep + "output"
    out_put_folder = base_out_put_folder + os.sep + time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
    if os.path.exists(out_put_folder):
        # shutil.rmtree 递归删除目标目录文件夹下的所有子文件夹和子文件，包括目标文件夹
        shutil.rmtree(out_put_folder)
    os.makedirs(out_put_folder)

    main("-env prod -i smoke -t ./tests -n 8")

    end = time.time()
    log.info('本次总运行时间 %s s' % (end - start))

    log.info('\nTotal %s cases were run:' % (len(cases_run_success) + len(cases_run_fail) + len(cases_encounter_error)))

    if cases_run_success:
        log.info('\033[1;32m')
        log.info('%s cases Passed, details info are:\n %s' % (
            len(cases_run_success), list(map(lambda x: x["testcase"], cases_run_success))))
        log.info('\033[0m')

    if cases_run_fail:
        log.info('%s cases Failed, details info are:\n %s' % (
            len(cases_run_fail), list(map(lambda x: x["testcase"], cases_run_fail))))
        log.error('\033[1;31m')
    if cases_encounter_error:
        log.info('%s cases Run error, details info are:\n %s' % (
            len(cases_encounter_error), list(map(lambda x: x["testcase"], cases_encounter_error))))
        log.error('\033[0m')

    if skipped_cases:
        log.info('%s cases Skipped, details info are::\n %s' % (
            len(skipped_cases), list(map(lambda x: x["testcase"], skipped_cases))))

    log.info("\n{pre_fix} Tests done {timer}, "
             "total used {c_time:.2f} seconds {pre_fix} ".format(pre_fix='---' * 10,
                                                                 timer=time.strftime(
                                                                     '%Y-%m-%d %H:%M:%S',
                                                                     time.localtime(
                                                                         end)),
                                                                 c_time=end - start))

    # Generate automation report
    log.info("\n{pre_fix} Starting to generate automation report... {pre_fix} ".format(pre_fix='---' * 10))
    generate_html_report(out_put_folder, cases_run_success, cases_run_fail, cases_encounter_error, skipped_cases,
                         time.time(), platform.system(), False)
    log.info("\n{pre_fix} Generate report done, please check report.html from '{report}'. {pre_fix} ".format(
        pre_fix='---' * 10, report=out_put_folder))
