# @Time : 2022/3/13 11:38 AM 
# @Author : SailYang
import time

from selenium.webdriver.common.by import By

from ReportTestDemo.configs.global_config import get_config
from ReportTestDemo.pages.base_page import BasePage
from ReportTestDemo.utilities.yaml_hepler import YamlHelper


class Baidu(BasePage):
    DOMAIN = get_config('config')['DOMAIN']

    element_locator_yaml = './configs/element_locator/baidu.yaml'
    element = YamlHelper.read_yaml(element_locator_yaml)

    input_box = (By.ID, element['KEY_WORLD_LOCATOR'])
    search_btn = (By.ID, element['SEARCH_BUTTON_LOCATOR'])
    first_result = (By.XPATH, element['FIRST_RESULT_LOCATOR'])

    def __init__(self, driver=None):
        super().__init__(driver)

    def baidu_search(self, search_string):
        self.driver.get(self.DOMAIN)
        self.driver.find_element(*self.input_box).clear()
        self.driver.find_element(*self.input_box).send_keys(search_string)
        self.driver.find_element(*self.search_btn).click()
        time.sleep(5)
        search_results = self.driver.find_element(*self.first_result).get_attribute('innerHTML')
        return search_results
