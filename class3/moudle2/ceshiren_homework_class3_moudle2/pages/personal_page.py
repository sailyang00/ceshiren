# @Author : SailYang
# @Time : 2021/7/22 17:17
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.base import Base
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.managepersonal_page import ManagePersonalPage


class PersonalPage(Base):
    """
    个人管理页
    """
    _manage_button = "//*[@class='android.widget.LinearLayout' and " \
                     "@index=2 and @resource-id='com.tencent.wework:id/isz']"

    def goto_manage_personal(self):
        # 等待右上角更多按钮出现
        self.wait_element(self._manage_button)
        # 点击跳转到个人操作管理页
        self.find(self._manage_button, click=True)
        return ManagePersonalPage(self.driver)
