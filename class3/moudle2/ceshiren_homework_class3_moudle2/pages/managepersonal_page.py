# @Author : SailYang
# @Time : 2021/7/22 17:27
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.base import Base
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.editpersonal_page import EditPersonalPage


class ManagePersonalPage(Base):
    """
    个人操作管理页
    """
    _edit_button = "//*[@text='编辑成员']"

    def goto_edit_personal(self):
        # 点击跳转编辑个人管理页
        self.find(self._edit_button, click=True)
        return EditPersonalPage(self.driver)
