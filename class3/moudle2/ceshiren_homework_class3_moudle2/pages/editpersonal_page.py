# @Author : SailYang
# @Time : 2021/7/22 17:32
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.base import Base


class EditPersonalPage(Base):
    """
    编辑个人管理页
    """
    _cal_button = "//*[@text='删除成员']"
    _cal_confirm_button = "//*[@text='确定']"
    _my_name = "//*[@text='杨宇帆']/../../../../../..//*[@class='android.widget.TextView']"
    _set_department_ele = "//*[contains(@text,'设置部门')]"

    def cal_member(self):
        """
        该方法是删除人员
        """
        # 拖动屏幕，找到最下方的删除按钮，并点击
        self.move_touch(
            4 / 5,
            4 / 5,
            1 / 2,
            1 / 2, self._set_department_ele)
        self.find(self._cal_button, click=True)
        self.wait_element(self._cal_confirm_button)
        self.find(self._cal_confirm_button, click=True)
        from class3.moudle2.ceshiren_homework_class3_moudle2.pages.addresslist_page import AddressListPage
        return AddressListPage(self.driver)
