# @Author : SailYang
# @Time : 2021/7/21 16:06
from time import sleep

from class3.moudle2.ceshiren_homework_class3_moudle2.pages.addmember_page import AddMemberPage
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.base import Base
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.personal_page import PersonalPage


class AddressListPage(Base):
    """
    通讯录
    """
    _add_member_button = "//*[@text='添加成员' and @class ='android.widget.TextView']"
    _member_num = "//*[contains(@text,'人未加入'))]"
    _my_name = "//*[@text='杨宇帆']/../../../../../..//*[@class='android.widget.TextView']"

    def goto_add_member(self):
        """
        点击去往人员管理页
        """
        # 拖动页面，防止名单过长查找不到添加人员按钮
        self.move_touch(
            4 / 5,
            4 / 5,
            1 / 2,
            1 / 2)
        self.find(self._add_member_button, click=True)
        return AddMemberPage(self.driver)

    def goto_personal_page(self, name):
        # 查找通讯录名单
        name_list_elements, name_list = self.name_list_elements(self._my_name)
        print(name_list)
        # 判断name 是否再通讯录内
        if name in name_list:
            _name = f"//*[contains(@text,'{name}')]"
            self.find(_name, click=True)
            # 若存在就去该人员的个人管理页
            return PersonalPage(self.driver)
        else:
            # 不存在抛出异常
            raise Exception('删除失败，不存在该人员')

    def check_member(self, name):
        """
        该方法用于检查删除人员是否还在通讯录中
        """
        # 强制等待：删除成功后，页面没有特征可以捕捉，只能强制等待
        sleep(2)
        # 判断name 是否再通讯录内
        name_list_elements, name_list = self.name_list_elements(self._my_name)
        if name not in name_list:
            # 不存在返回 True
            return True
        else:
            # 存在返回 False
            return False
