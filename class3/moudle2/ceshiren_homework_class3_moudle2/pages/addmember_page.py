# @Author : SailYang
# @Time : 2021/7/21 16:08
from time import sleep

from appium.webdriver.common.mobileby import MobileBy

from class3.moudle2.ceshiren_homework_class3_moudle2.pages.base import Base
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.editmember_page import EditMemberPage


class AddMemberPage(Base):
    """
    人员管理页
    """
    _add_member_handmade_button = "//*[@text='手动输入添加' and @class ='android.widget.TextView']"
    _wait_toast_ele = '//*[contains(@text,"添加成功")]'
    # _wait_toast_ele = (MobileBy.XPATH, '//*[@class="android.widget.Toast"]')

    def click_add_member_menu(self):
        """
        点击跳转人员编辑页
        """
        self.find(self._add_member_handmade_button, click=True)
        return EditMemberPage(self.driver)

    def get_toast(self):
        """
        获取添加功能提示
        """
        print(self.driver.current_package)
        print(self.driver.current_activity)
        self.wait_element(self._wait_toast_ele)
        return True
