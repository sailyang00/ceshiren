# @Author : SailYang
# @Time : 2021/7/21 15:31
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.addresslist_page import AddressListPage
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.base import Base


class MainPage(Base):
    """
    主页
    """
    _address_button = "//*[@text='通讯录' and @class ='android.widget.TextView']"

    def goto_address_list(self):
        # 点击去往通讯录
        self.find(self._address_button, click=True)

        return AddressListPage(self.driver)
