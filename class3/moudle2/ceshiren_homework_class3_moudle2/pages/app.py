# @Author : SailYang
# @Time : 2021/7/21 15:06
from appium import webdriver
from appium.webdriver.webdriver import WebDriver

from class3.moudle2.ceshiren_homework_class3_moudle2.pages.base import Base
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.main_page import MainPage


class App(Base):
    """
    app 启动管理页
    """
    def start(self, driver=None):
        # 若driver不存在,生成driver
        if driver is None:
            des_caps = dict(
                platformName="android",
                platformVersion="6.0.1",
                # browserName="Browser",
                appPackage="com.tencent.wework",
                appActivity=".launch.WwMainActivity",
                # 保存缓存操作
                noReset=True,
                # 首次启动不重启app
                dontStopAppOnReset=True,
                # 跳过安装
                skipDeviceInitialization=True,
                # 使用自带的输入法,允许输入中文
                unicodeKeyboard=True,
                # 执行完恢复原来的输入法
                resetKeyboard=True,
                # 自动授权
                autoGrantPermissions=True,
                deviceName="127.0.0.1:7555",
                # automationName='uiautomator2',

                # 到默认的路径去查找
                # chromedriverExecutable='默认路径'
            )

            self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', des_caps)
        else:
            # 若driver存在,直接启动
            self.driver.launch_app()

        self.driver.implicitly_wait(10)
        return self

    def restart(self):
        pass

    def stop(self):
        # self.driver.quit()
        pass

    def goto_main(self):
        return MainPage(self.driver)
