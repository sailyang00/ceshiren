# @Author : SailYang
# @Time : 2021/7/21 16:11
from class3.moudle2.ceshiren_homework_class3_moudle2.pages.base import Base


class EditMemberPage(Base):
    """
    人员管理页
    """
    _name_input_ele = "//*[contains(@text,'姓名')]/..//*[@text='必填']"
    _number_input_ele = "//*[contains(@text,'手机')]/..//*[@text='必填']"
    _add_member_save = "//*[@text='保存' and @class='android.widget.TextView']"

    def add_member(self, name, phone_number):
        """
        点击添加按钮，跳转添加人员页面
        """
        self.find(self._name_input_ele, text=f'{name}')
        self.find(self._number_input_ele, text=f'{phone_number}')
        self.find(self._add_member_save, click=True)

        from class3.moudle2.ceshiren_homework_class3_moudle2.pages.addmember_page import AddMemberPage
        return AddMemberPage(self.driver)
