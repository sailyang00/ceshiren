# @Author : SailYang
# @Time : 2021/7/21 15:27
import logging
import time
from functools import wraps

from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

logger = logging.getLogger(__name__)


def log(func):
    """
    封装log装饰器，打印每个参数和方法的执行日志
    """
    # wraps 防止覆盖原来的方法名
    # func 相当于被装饰的方法名
    @wraps(func)
    def wrapper(*args, **kwargs):
        # func(*args, **kwargs) 相当于执行方法名和其形参，生成了对象ret
        ret = func(*args, **kwargs)
        # 打印方法名，入参参数数据
        logger.info((f"function:{func.__name__}", args, ', '.join([f'{k}={v}' for k, v in kwargs.items()])))
        # 返回方法对象
        return ret
    # 返回wrapper
    return wrapper


class Base:
    def __init__(self, driver: WebDriver = None):
        self.driver = driver

    @log
    def find(self, locate, click=False, text=None, plural=False, nums=0):
        # 当查找的不是多个元素时
        if plural is False:
            ele = self.driver.find_element(MobileBy.XPATH, locate)
        # 当查找的多个元素时，返回列表
        elif nums is None and plural is True:
            ele = self.driver.find_elements(MobileBy.XPATH, locate)
        # 当查找多个元素时，默认取第一个。nums可设置
        elif plural is True:
            ele = self.driver.find_elements(MobileBy.XPATH, locate)[nums]
        # 当元素不需要点击和填写text时
        if click is False and text is None:
            return ele
        # 当元素要点击时
        elif click is True and text is None:
            ele.click()
        # 当元素填写text时
        elif text:
            ele.send_keys(text)
        # 即要点击和填写信息
        else:
            ele.send_keys(text)
            ele.click()

    @log
    def wait_element(self, locate):
        # 显式等待 可见元素
        return WebDriverWait(self.driver, 10).until(lambda x: x.find_element(MobileBy.XPATH, locate))

    @log
    def back(self):
        """
        后退键
        """
        self.driver.back()

    @log
    def current_activity(self):
        """
        获取当前页面的名字
        """
        return self.driver.current_activity

    @log
    def name_list_elements(self, _cal_member):
        """
        获取通讯录名单集合，name_list
        获取通讯录名单元素集合，name_list_elements
        """
        name_list = []
        name_list_elements = self.find(_cal_member, plural=True, nums=None)
        for ele in name_list_elements:
            name_list.append(ele.get_attribute('text'))
        return name_list_elements, name_list

    @log
    def move_touch(self, x1, y1, x2, y2, locate=None):
        """
        拖动页面,以抓取到locate为准，也可以不传
        """
        if locate:
            self.wait_element(locate)
        # 获取手机屏幕尺寸
        window_rect = self.driver.get_window_rect()
        width = window_rect['width']
        height = window_rect['height']
        x_1 = int(width * x1)
        x_2 = int(width * x2)
        y_1 = int(height * y1)
        y_2 = int(height * y2)
        # 调用swipe方法
        self.driver.swipe(x_1, y_1, x_2, y_2)
