# @Author : SailYang
# @Time : 2021/7/23 17:08

from faker import Faker


class AndroidUtils:
    """
    生成faker账号和手机工具
    """
    name = []
    phone = []

    def _name_list(self, num):
        faker = Faker('zh_CN')
        for i in range(num):
            _name = faker.name()
            _phone = faker.phone_number()
            self.name.append(_name)
            self.phone.append(_phone)
        return zip(self.name, self.phone)

    def name_list(self, num):
        """
        输入num控制生成名单数量
        """
        name_list = [i for i in self._name_list(num)]
        # 返回faker名单
        return name_list


if __name__ == '__main__':
    AndroidUtils().name_list(3)
