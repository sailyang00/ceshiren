# @Author : SailYang
# @Time : 2021/7/23 17:37
import datetime
import logging

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(filename)-8s - %(module)-8s - %(funcName)-14s - %(lineno)d - %(levelname)-1s - %(message)-12s',
                    datefmt='%Y-%m-%d-%H-%M-%S',
                    filename='../log/' + __name__ + '.log',
                    filemode='w'
                    )


def pytest_collection_modifyitems(session, config, items):
    print(items)
    for item in items:
        print(item)
        # 将用例名称和路径重新编码
        item.name = item.name.encode('utf-8').decode('unicode-escape')
        item._nodeid = item.nodeid.encode('utf-8').decode('unicode-escape')
