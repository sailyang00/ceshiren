# @Author : SailYang
# @Time : 2021/7/21 15:24
import allure
from faker import Faker
import pytest

from class3.moudle2.ceshiren_homework_class3_moudle2.pages.app import App
from class3.moudle2.ceshiren_homework_class3_moudle2.utils.android_utils import AndroidUtils


@allure.feature('企业微信自动化测试')
class TestAndroid:
    def setup_class(self):
        self.app = App()
        self.main = App().start()
        self.faker = Faker('zh_CN')

    def teardown(self):
        # 查询当前页面是否为主页，否则一直操作后退键
        while 'launch' not in self.main.current_activity():
            self.main.back()

    @allure.story('增加人员')
    @pytest.mark.parametrize('name,phone', AndroidUtils().name_list(1))
    def test_add_member_case(self, name, phone):
        result = self.main.goto_main().goto_address_list().goto_add_member(). \
            click_add_member_menu().add_member(name, phone).get_toast()
        assert result

    @allure.story('删除人员')
    @pytest.mark.parametrize('name', ['曹芳', '陈宇'])
    def test_cal_member_case(self, name):
        result = self.main.goto_main().goto_address_list().goto_personal_page(name).goto_manage_personal(). \
            goto_edit_personal().cal_member().check_member(name)
        assert result


if __name__ == '__main__':
    pytest.main([
        '-vs',
        'test_android.py'
    ])
