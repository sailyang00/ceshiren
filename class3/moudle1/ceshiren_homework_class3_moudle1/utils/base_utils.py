# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/7/4 4:08 下午
import shelve
from time import sleep

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from class3.moudle1.ceshiren_homework_class3_moudle1.page.base import Base


class BaseUtils(Base):
    """
    工具类：
    get_mydbs_cookies ：从数据库shelve获取cookies
    add_mydbs_cookies ：添加cookies到web服务器
    load_mydbs_cookies ： 更新cookies到shelve数据库
    set_message_script ：设置弹窗消息
    set_click_script ：执行script点击操作
    find_ele ：查找元素，集合了点击，填写消息，查找多个元素情况
    wait_element： 等待元素
    driver_quit ：关闭web服务器

    """
    _mydabs_cookies = '../mydbs/cookies'

    def get_mydbs_cookies(self):
        # 打开数据库
        with shelve.open(self._mydabs_cookies) as db:
            try:
                # 读取cookies数据
                cookies = db['cookies']
                # 如果读取失败，比如cookies为空时
            except KeyError:
                # 就从web读取数据
                cookies = self.driver.get_cookies()
        # 返回数据
        return cookies

    def add_mydbs_cookies(self, cookies) -> bool:
        # 当cookies有数据时
        if cookies:
            # 把cookies加载到web上
            for cookie in cookies:
                self.driver.add_cookie(cookie)
        else:
            # 如果cookies为空 ，返回None
            return None

    def load_mydbs_cookies(self):
        # 获取cookies
        cookies = self.driver.get_cookies()
        # 更新数据库cookies信息
        with shelve.open(self._mydabs_cookies) as db:
            db['cookies'] = cookies
            return db['cookies']

    def set_message_script(self, timeout: int, message: str):
        # 执行script，alert弹窗提示消息
        self.driver.execute_script(f"alert('{message}')")
        # 设置弹窗停留时间
        sleep(timeout)
        # 如果手动关闭，不做处理
        try:
            self.driver.switch_to.alert.accept()
            self.driver.switch_to.default_content()
        except Exception:
            pass

    def set_click_script(self, value):
        # 执行script 点击操作，主要是靠find_ele 方法稍微麻烦了点，当同名元素较多时使用该方法
        self.driver.execute_script(f"document.getElementsByClassName('{value}')[0].click()")

    def find_ele(self, locate: tuple, click=False, text=None, plural=False, nums=0):
        # 当查找的不是多个元素时
        if plural is False:
            ele = self.driver.find_element(*locate)
        # 当查找的多个元素时，返回列表
        elif nums is None and plural is True:
            ele = self.driver.find_elements(*locate)
        # 当查找多个元素时，默认取第一个。nums可设置
        elif plural is True:
            ele = self.driver.find_elements(*locate)[nums]
        # 当元素不需要点击和填写text时
        if click is False and text is None:
            return ele
        # 当元素要点击时
        elif click is True and text is None:
            ele.click()
        # 当元素填写text时
        elif text:
            ele.send_keys(text)
        # 即要点击和填写信息
        else:
            ele.send_keys(text)
            ele.click()

    def wait_element(self, locate):
        try:
            # 显式等待 可见元素
            WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located(locate))
        except TimeoutError:
            # 超过30秒报错
            raise Exception("30s内超时未扫码")

    def driver_quit(self):
        # 关闭web浏览器
        self.driver.quit()
