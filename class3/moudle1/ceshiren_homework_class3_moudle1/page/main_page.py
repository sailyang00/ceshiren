# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/7/4 4:01 下午
from selenium.webdriver.common.by import By

from class3.moudle1.ceshiren_homework_class3_moudle1.page.create_members_page import CreateMembersPage
from class3.moudle1.ceshiren_homework_class3_moudle1.page.members_page import MembersPage
from class3.moudle1.ceshiren_homework_class3_moudle1.utils.base_utils import BaseUtils


class MainPage(BaseUtils):
    """
    主页类:
    click_to_member_page  点击添加成员，跳转到添加成员页面
    click_to_create_member_page  点击管理成员，跳转到管理成员页面
    """
    _add_mem_locate = (By.XPATH, '//div[@id="_hmt_click"]/div[1]/div[4]/div[2]/a[1]/div/span[2]')
    _to_mem_locate = (By.ID, "menu_contacts")

    def click_to_member_page(self):
        # 查找元素
        self.find_ele(self._to_mem_locate, click=True)
        return MembersPage(self.driver)

    def click_to_create_member_page(self):
        self.find_ele(self._add_mem_locate, click=True)
        return CreateMembersPage(self.driver)
