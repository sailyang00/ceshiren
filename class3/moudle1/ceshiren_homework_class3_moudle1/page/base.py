# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/7/4 12:28 下午

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver


class Base:
    """
    基础类：用来初始化webdriver
    """
    _base_url = ""
    _mydabs_cookies = '../mydbs/cookies'

    def __init__(self, base_driver=None):
        # 判断传进来的base_driver是否 self.driver ，是则重新赋值给self.driver。
        # 主要是为了其他类继承Base不会重启web服务器
        if base_driver is None:
            self.driver = webdriver.Chrome()
            self.driver.get(self._base_url)
            self.driver.maximize_window()
            # 隐式等待
            self.driver.implicitly_wait(5)
        else:
            self.driver: WebDriver = base_driver

        # 用于Debugger模式，不关闭浏览器
        # chrome_option = webdriver.ChromeOptions()
        # chrome_option.debugger_address = '127.0.0.1:9222'
        # self.driver = webdriver.Chrome(options=chrome_option)
