# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/7/6 11:05 上午
from selenium.webdriver.common.by import By

from class3.moudle1.ceshiren_homework_class3_moudle1.page.members_page import MembersPage
from class3.moudle1.ceshiren_homework_class3_moudle1.utils.base_utils import BaseUtils


class CreateMembersPage(BaseUtils):
    """
    创建成员页
    click_to_members_page ：点击保存按钮，跳转成员管理页

    """
    _name_locate = (By.ID, 'username')
    _phone_number_locate = (By.ID, 'memberAdd_phone')
    _other_name_locate = (By.XPATH, '//input[@tabindex=3]')
    # 用尽find_element方法都定位不到，只能使用最后的绝招：执行script
    # _btn_save_locate = (By.CLASS_NAME, 'qui_btn ww_btn js_btn_save')
    _gender_locate = (By.CSS_SELECTOR, '.ww_radio[value="1"][checked]')
    _btn_save_value = 'qui_btn ww_btn js_btn_save'

    def click_to_members_page(self, name):
        # 填上一些必填数据，保存
        self.find_ele(self._name_locate, text=name)
        self.find_ele(self._other_name_locate, text='3123')
        self.find_ele(self._phone_number_locate, text='18636478108')
        self.find_ele(self._gender_locate, click=True)
        self.set_click_script(self._btn_save_value)
        # 返回到成员管理页
        return MembersPage(self.driver)
