# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/7/4 12:37 下午


from selenium.webdriver.common.by import By

from class3.moudle1.ceshiren_homework_class3_moudle1.page.main_page import MainPage
from class3.moudle1.ceshiren_homework_class3_moudle1.utils.base_utils import BaseUtils


class LoginPage(BaseUtils):
    """
    LoginPage 登录页，主要用于自动化登录，如果cookies过期，需要手动进行扫码。扫码过后会自动更新cookies
    继承了base_driver，该继承类里封装了各式各样的工具和方法
    """
    _base_url = 'https://work.weixin.qq.com/wework_admin/loginpage_wx'
    _locator = (By.CLASS_NAME, 'index_service_cnt_item_title')

    def login_to_main_page(self):
        # 从数据库shelve 中获取cookies
        cookies = self.get_mydbs_cookies()
        # 更新cookies到shelve
        self.add_mydbs_cookies(cookies)

        # 刷新页面
        self.driver.refresh()
        # 判断是否登录成功后的链接，current_url获取当前链接
        if not "frame" in self.driver.current_url:
            # 执行script
            self.set_message_script(timeout=5, message="cookies过期，请重新扫码")
            # 等待元素
            self.wait_element(locate=self._locator)
            # 更新cookies到shelve
            self.load_mydbs_cookies()
        # 返回MainPage实例，并传入self.driver，目的是为了不重启浏览器
        return MainPage(self.driver)
