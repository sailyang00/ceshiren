# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/7/5 5:37 下午
from time import sleep

from selenium.webdriver.common.by import By

from class3.moudle1.ceshiren_homework_class3_moudle1.utils.base_utils import BaseUtils


class MembersPage(BaseUtils):
    """
    MembersPage 管理成员页
    """
    _base_url = 'https://work.weixin.qq.com/wework_admin/frame#contacts'
    _find_ele_locate = (By.CSS_SELECTOR, 'td:nth-child(2)')
    _click_to_detele_classname = 'qui_btn ww_btn js_delete'
    _click_confirm_locate = (By.XPATH, '//a[@d_ck="submit"]')
    _detele_success_locate = (By.ID, "js_tips")

    def check_members(self):
        # 查找成员列表中有哪些成员
        self.datas = [ele.get_attribute('title') for ele in
                      self.find_ele(self._find_ele_locate, plural=True, nums=None)]
        print(self.datas)
        # 返回成员列表
        return self.datas

    def detele_test_datas(self, name):
        # 判断传入的名字在不在成员列表中
        if name in self.check_members():
            # 如果存在则执行删除操作
            _detele_ele_locate = (By.XPATH, f"//td[@title='{name}']//preceding-sibling::td")
            self.find_ele(_detele_ele_locate, click=True)
        else:
            # 如果输入不存在的名单报错
            raise Exception("要删除的名单不存在")
        # 执行script 点击操作：点击删除按钮
        self.set_click_script(self._click_to_detele_classname)
        # 等待元素 二次确认弹窗的确认按钮
        self.wait_element(self._click_confirm_locate)
        # 查找元素  确认按钮
        self.find_ele(self._click_confirm_locate, click=True)
        # 等待 删除成功提示（不一定成功，系统操作太快）
        self.wait_element(self._detele_success_locate)
        # 刷新页面（保险措施）
        self.driver.refresh()
        # 返回成员列表
        return self.check_members()
