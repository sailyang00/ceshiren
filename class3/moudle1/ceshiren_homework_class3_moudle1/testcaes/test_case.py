# -*- coding:utf-8 -*-
# author:Sailyang
# time:2021/7/4 12:35 下午

import pytest

from class3.moudle1.ceshiren_homework_class3_moudle1.page.login_page import LoginPage


class TestCase:

    @pytest.mark.parametrize('name', ['hello_world'])
    def test_login(self, name):
        self.test_case = LoginPage()
        # 判断hello_world 用户在不在 用户列表中
        assert name in self.test_case.login_to_main_page().click_to_create_member_page().click_to_members_page(
            name).check_members()

    @pytest.mark.parametrize('name', ['hello_world'])
    def test_delete(self, name):
        self.test_case = LoginPage()
        # 判断hello_world 是否已被删除
        assert name not in self.test_case.login_to_main_page().click_to_member_page().detele_test_datas(name)

    def teardown(self):
        # 用例执行完毕后关闭浏览器
        self.test_case.driver_quit()


if __name__ == '__main__':
    pytest.main(['-vs', 'test_case.py'])
