# @Time : 2022/3/6 6:06 PM 
# @Author : SailYang
import importlib.util
import inspect
import sys

from ddt import mk_test_name


class DiscoverTestCases:
    def __init__(self, test_file=None):
        self.test_file = test_file

    def find_test_module(self):
        """
        根据指定文件查找到测试文件名并将其导入
        :return: 返回mod_ref 测试文件列表
        """
        mod_ref = []

        # 将测试文件名查找出来并放在list中（不包括文件拓展名）
        module_name_list = [inspect.getmodulename(self.test_file)]
        module_file_paths = [self.test_file]

        """
        zip(a,b)返回值为对象，需要将返回值转成list(),结构为[(测试数据1,测试数据2),(测试数据3),(测试数据4)]
        """
        for module_name, module_file_path in zip(module_name_list, module_file_paths):
            try:
                # importlib 可以通过仅传入文件名和其路径就可以导入模块
                # spec_from_file_location()接受文件名和路径，返回导入模块名
                module_spec = importlib.util.spec_from_file_location(module_name, module_file_path)

                # 通过module_from_spec方法可以判断 传入的模块是否存在，如果存在返回该模块对象，反之返回None
                module = importlib.util.module_from_spec(module_spec)

                # 通过loader.exec_module方法 传入模块对象，将模块导入项目环境
                module_spec.loader.exec_module(module)

                """
                sys.modules 是一个全局字典，每当导入新模块就会记录模块名字，
                并且起到了缓存作用，当再次导入相同模块时会从字典中查找从而加快程序运行速度
                """
                # 将module_name和module导入模块以字典的形式存入到sys.modules全局字典中
                sys.modules[module_name] = module
                mod_ref.append(module)
            except ImportError:
                raise ImportError(f'Module:{self.test_file} can not imported')
        return mod_ref

    def find_tests(self, mod_ref):
        """

        :param mod_ref: 入参，测试文件列表
        :return:返回测试用例集合
        """
        test_cases = []
        for module in mod_ref:

            # inspect.getmembers() 可以获取对象的属性
            # 如：class,method,function,generator,tracback,frame,code,builtin，返回值为(name,value)的列表组合
            # inspect.isfunction() 判断传入对象是否为函数
            cls_members = inspect.getmembers(module, inspect.isclass)
            for cls in cls_members:
                cls_name, cls_code_objects = cls
                # 查找仅"tests"开头的测试用例的对象
                for func_name in dir(cls_code_objects):
                    if func_name.startswith('test'):
                        tests_suspect = getattr(cls_code_objects, func_name)

                        # 判断测试用例是否存在 __data_Provider__ 属性
                        if hasattr(tests_suspect, "__data_Provider__"):
                            """
                            setattr(func, "__data_Provider__", test_data)
    
                            getattr(tests_suspect, "__data__Provider") 由此可知这里获取test_data，
                            test_data就是数据驱动装饰器传入的数据参数
                            """
                            # 把传入的数据参数进行枚举分组
                            for i, v in enumerate(getattr(tests_suspect, "__data_Provider__")):
                                # mk_test_name为ddt工具提供的方法，用于生成新的测试用例名称

                                new_test_name = mk_test_name(tests_suspect.__name__, getattr(v, "__name__", v), i)

                                # 将测试类，原始的测试用例名称，新的测试用例名称，测试用例数据组成一条测试用例供测试框架后续调用
                                test_cases.append((cls_name, tests_suspect.__name__, new_test_name, v))
                        else:

                            # 当没有 __data__Provider 属性直接返回原测试用例
                            test_cases.append((cls_name, func_name, func_name, None))
        return test_cases
