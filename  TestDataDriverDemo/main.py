# @Time : 2022/3/6 11:52 PM 
# @Author : SailYang
import inspect
from common.test_case_finder import DiscoverTestCases
import tests.test_demo

if __name__ == '__main__':
    # 获取测试文件地址
    test_file_path = inspect.getfile(tests.test_demo)

    # 生成DiscoverTestCases实例
    case_finder = DiscoverTestCases(test_file_path)

    # 查找测试模块并导入
    test_module = case_finder.find_test_module()

    # 查找测试用例
    test_cases = case_finder.find_tests(test_module)
    for i in test_cases:
        print(i)
