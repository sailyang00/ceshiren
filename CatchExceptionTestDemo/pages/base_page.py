# @Time : 2022/3/13 11:38 AM 
# @Author : SailYang
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver


class BasePage(object):
    def __init__(self, driver):
        if driver:
            self.driver = driver
        else:
            self.driver: WebDriver = webdriver.Chrome()

    def open_page(self, url):
        self.driver.get(url)

    def close(self):
        self.driver.close()
