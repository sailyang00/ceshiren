# @Time : 2022/3/12 11:00 PM 
# @Author : SailYang
import functools
import subprocess
from collections import OrderedDict
from multiprocessing.pool import ThreadPool
import platform
from time import time

from CatchExceptionTestDemo.common.customize_error import RunTimeTooLong
from CatchExceptionTestDemo.common.test_case_finder import DiscoverTestCases
from CatchExceptionTestDemo.common.test_filter import TestFilter
from CatchExceptionTestDemo.common.user_options import parser_options
from CatchExceptionTestDemo.configs.global_config import config_init, set_config, get_config


def kill_process_by_name(process_name):
    try:
        if platform.system() == "Windows":
            subprocess.check_output(f"taskkill /f /im {process_name}", shell=True, stderr=subprocess.STDOUT)

        # Mac
        elif platform.system() == "Darwin":
            subprocess.check_output(f"ps -ef | grep -i {process_name} | awk {'print $2'} | awk 'NR==1'|xargs kill -9",
                                    shell=True, stderr=subprocess.STDOUT)
        else:
            # 如果是mac的话
            subprocess.check_output(f"killall {process_name}", shell=True, stderr=subprocess.STDOUT)
    except BaseException as e:
        print(e)


def clear_env():
    if platform.system() == "Windows":
        kill_process_by_name("chrome.exe")
        kill_process_by_name("chromedriver.exe")
        kill_process_by_name("firefox.exe")
        kill_process_by_name("iexplore.exe")
        kill_process_by_name("IEDriverServer.exe")
    else:
        kill_process_by_name("Google Chrome")
        kill_process_by_name("chromedriver")


def group_test_cases_by_class(cases_to_run):
    """
    dict字典是无序的，所以存储的数据再迭代时会随机取值，导致顺序随机
    collections.OrderedDict() 方法可以对字典按顺序存取
    """
    test_group_dict = OrderedDict()
    for item in cases_to_run:
        tag_filter, cls, func_name, func, value = item

        # setdefault() 设置键和值的类型，并按顺序存入
        test_group_dict.setdefault(cls, []).append((tag_filter, cls, func_name, func, value))

        # 将测试数据按顺序取出
        test_groups = [(x, y) for x, y in zip(test_group_dict.keys(), test_group_dict.values())]
    return test_groups


def class_run(case, test_thread_number):

    # 并发寻找测试用例
    print(case)
    cls, func_pack = case
    p = ThreadPool(test_thread_number)
    p.map(func_run, func_pack)
    p.close()
    p.join()


def func_run(case):
    try:
        # 开始时间
        s = time()
        cls_group_name, cls, func_name, func, value = case
        cls_instance = cls()
        if value:
            getattr(cls_instance, func.__name__).__wrapped__(cls_instance, *value)
        else:
            getattr(cls_instance, func.__name__).__wrapped__(cls_instance)
        # 运行结束时间
        e = time()
        if e - s > get_config('config')["run_time_out"]:
            raise RunTimeTooLong(func_name, e - s)
    except RunTimeTooLong as runtime_err:
        print(runtime_err)

        # 当出现RunTimeTooLong错误时，设置相应的属性
        setattr(func, 'run_status', 'error')
        setattr(func, 'exception_type', 'RunTimeLong')
    except AssertionError as assert_err:
        print(assert_err)
    except Exception as e:

        # 将异常信息染色
        print(f'\033[1;31;40m{e}\033[0m')
    finally:
        # 环境清理
        clear_env()


def main(args=None):
    start = time()
    options = parser_options(args)

    # 初始化全局变量
    config_init()

    # 设置全局变量
    set_config('config', options.config)

    # 从默认文件tests查找测试用例
    case_finder = DiscoverTestCases()

    # 查找到测试模块并导入
    test_module = case_finder.find_test_module()

    # 查找并筛选测试用例
    original_test_cases = case_finder.find_tests(test_module)

    # 根据用户命令行输入 -i 参数进一步筛选
    raw_test_suites = TestFilter(original_test_cases).tag_filter_run(options.include_tags_any_match)

    # 获取最终的测试用例，并按类名组织
    test_suites = group_test_cases_by_class(raw_test_suites)

    print(f"运行线程数为:{options.test_thread_number}")

    p = ThreadPool(options.test_thread_number)

    # functools.partial 偏函数
    # functools.partial(func,x)，x为func的入参参数 ，在map函数里 test_suites 也是func的入参
    # class_run 也是线程池并发，所以这里有两层并发，第一层是查找测试模块，第二层是查找测试用例
    p.map(
        functools.partial(
            class_run,
            test_thread_number=options.test_thread_number
        ), test_suites
    )
    p.close()
    p.join()
    end = time()
    print(f"本次总运行时间{end - start}")


if __name__ == '__main__':
    main("-env prod -i smoke -t ./tests")
