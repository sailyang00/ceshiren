# @Time : 2022/2/24 9:41 PM 
# @Author : SailYang
import allure
import pytest


# 当用户不传flag参数时，默认为false；不传browser参数时，默认为Chrome
def pytest_addoption(parser):
    parser.addoption(
        "--flag", action="store_true", default=False, help="set skip or not"
    )
    parser.addoption(
        "--browser", action="store", default="Chrome", help="ser browser"
    )


# 获取flag的值
@pytest.fixture(scope='session')
def get_flag(request):
    return request.config.getoption('--flag')


# 获取browser的值
@pytest.fixture(scope='session')
def get_browser(request):
    return request.config.getoption('--browser')


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    """
    本钩子函数用于制作测试报告
    :param item: 测试用例对象
    :param call: 测试用例的测试步骤
            执行完常规钩子函数返回的report报告中有一个属性 report.when（前后会执行三次，所以有三个阶段：setup,call,teardown）
            when='setup' 代表返回setup的执行结果
            when='call' 代表返回的call的执行结果
    :return:
    """
    # yield：执行结果赋值到了yield
    outcome = yield

    # yield.ger_result() 为执行报告
    report = outcome.get_result()

    # 当用例失败或者测试被忽略执行时，自动截图
    if (report.when == 'call' or report.when == 'setup') and (report.failed or report.skipped):
        try:
            # item.fixturenames 为测试用例的方法名和入参的元组
            if "initial_browser" in item.fixturenames:

                # item.funcargs 则是测试用例的参数名和值的字典
                web_driver = item.funcargs["initial_browser"]
            else:
                # 如果找不到driver就直接返回，也就是None
                return
            # 在allure报告上贴上报错截图
            allure.attach(web_driver.get_screenshot_as_png(), name="wrong picture",
                          attachment_type=allure.attachment_type.PNG)
        except Exception as e:
            print("failed to take screenshot".format(e))
