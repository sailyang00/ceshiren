# @Time : 2022/2/24 9:41 PM 
# @Author : SailYang
import allure
from flaky import flaky

import pytest


@allure.epic("allure-demo")
@allure.description("测试模块1用来对模块1进行测试")
@allure.feature("测试模块1")
@allure.story("测试模块 1——story")
@allure.testcase("http://baidu.com")
@pytest.mark.basic
class TestBasic:
    @allure.step("测试步骤1-判断登录邓城")
    @allure.severity("BLOCKER")
    def test_login(self):
        """
        模拟测试用例
        """
        assert 1 == 1

    @flaky
    @allure.step("测试步骤2-查询余额")
    @allure.severity("normal")
    def test_savings(self):
        """
        模拟失败的测试用例
        """
        assert 1 == 0

    @allure.step("测试步骤2.1-查询余额")
    @allure.severity("normal")
    def test_savings(self):
        """
        模拟失败的测试用例
        """
        assert 1 == 0

    @allure.description("调试用，不执行")
    def test_savings(self):
        """
        模拟跳过测试用例
        """
        pytest.skip('调试测试用例')

    @allure.issue("http://xxxx", "此处之前有bug，bug号如上")
    @allure.step("测试步骤3-取现")
    def test_deposit(self):
        raise Exception('oops')

    @pytest.mark.xfail(condition=lambda: True, reason="this tests is expecting failure")
    def test_xfail_expected_failure(self):
        """
        期望失败
        """
        assert False

    @pytest.mark.xfail(condition=lambda: True, reason="this tests is expecting failure")
    def test_xfail_unexpected_failure(self):
        """
        期望失败却执行成功，会被标记为不期望的成功
        """
        assert True

    @allure.step("测试步骤4 -teardown")
    def test_skip_by_triggered_condition(self, get_flag):
        if get_flag == True:
            pytest.skip("flag是true时，跳过此条用例")
