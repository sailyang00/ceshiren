# @Time : 2022/2/24 9:41 PM 
# @Author : SailYang
import time

import allure
import pytest
from selenium import webdriver


@allure.epic('baidu')
@allure.description('测试百度的搜索功能')
@allure.severity('BLOCKER')
@allure.feature('百度搜索')
@allure.testcase('http://www.baidu.com')
@pytest.mark.baidu
class TestBaidu:
    @pytest.fixture
    def initial_browser(self, get_browser):
        if get_browser:
            if get_browser.lower() == 'Chrome':
                self.driver = webdriver.Chrome()
            elif get_browser.lower() == 'firefox':
                self.driver = webdriver.Firefox()
            else:
                self.driver = webdriver.Chrome()
        else:
            self.driver = webdriver.Chrome()
            self.driver.implicitly_wait(30)
        self.base_url = "http://www.baidu.com"
        yield self.driver
        self.driver.quit()

    @allure.title('测试百度搜索成功')
    @pytest.mark.parametrize('search_string,expect_string', [('iTesting', 'iTesting'), ('helloqa.com', 'iTesting')])
    def test_baidu_search(self, initial_browser, search_string, expect_string):
        driver = initial_browser
        driver.get(self.base_url + "/")
        driver.find_element_by_id("kw").send_keys(search_string)
        driver.find_element_by_id("su").click()
        # 仅做demo
        time.sleep(2)
        search_results = driver.find_element_by_xpath('//*[@id="1"]/h3/a').get_attribute('innerHTML')
        assert (expect_string in search_string) is True

    @allure.title('测试百度搜索失败')
    @pytest.mark.parametrize('search_string,expect_string', [('iTesting', 'isGood')])
    def test_baidu_search_fail(self, initial_browser, search_string, expect_string):
        driver = initial_browser
        driver.get(self.base_url + "/")
        driver.find_element_by_id("kw").send_keys(search_string)
        driver.find_element_by_id("su").click()
        time.sleep(2)
        search_results = driver.find_element_by_xpath('//*[@id="1"]/h3/a').get_attribute('innerHTML')
        assert (expect_string in search_string) is True


if __name__ == '__main__':
    pytest.main(['-m', 'baidu', '-s', '-v', '-k', 'test_baidu_search', 'test_baidu_fixture_sample.py'])
